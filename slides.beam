^ \documentclass[12pt]{beamer}
^ \usepackage{fontawesome5}
^ \usepackage{listings,xcolor,lstautogobble}
^ \definecolor{background_color}{RGB}{255, 255, 255}
^ \definecolor{string_color}    {RGB}{180, 156,   0}
^ \definecolor{identifier_color}{RGB}{  0,   0,   0}
^ \definecolor{keyword_color}   {RGB}{ 64, 100, 255}
^ \definecolor{comment_color}   {RGB}{  0, 117, 110}
^ \definecolor{number_color}    {RGB}{ 84,  84,  84}
^ \lstdefinestyle{15150code}{
^     basicstyle=\ttfamily,
^     numberstyle=\tiny\ttfamily\color{number_color},
^     backgroundcolor=\color{background_color},
^     stringstyle=\color{string_color},
^     identifierstyle=\color{identifier_color},
^     keywordstyle=\color{keyword_color},
^     commentstyle=\color{comment_color},
^     numbers=left,
^     frame=none,
^     columns=fixed,
^     tabsize=2,
^     breaklines=true,
^     keepspaces=true,
^     showstringspaces=false,
^     captionpos=b,
^     autogobble=true,
^     mathescape=true,
^     literate={~}{{$\thicksim$}}1
^              {~=}{{$\eeq$}}1
^ }
^ \lstdefinelanguage{sml}{
^     language=ML,
^     morestring=[b]",
^     morecomment=[s]{(*}{*)},
^     morekeywords={
^         bool, char, exn, int, real, string, unit, list, option,
^         EQUAL, GREATER, LESS, NONE, SOME, nil,
^         andalso, orelse, true, false, not,
^         if, then, else, case, of, as,
^         let, in, end, local, val, rec,
^         datatype, type, exception, handle,
^         fun, fn, op, raise, ref,
^         structure, struct, signature, sig, functor, where,
^         include, open, use, infix, infixr, o, print
^     }
^ }
^ % code inline
^ \catcode`~=11
^ \newcommand{\code}[2][]{{\sloppy
^ \ifmmode
^     \text{\lstinline[language=sml,style=15150code,#1]`#2`}
^ \else
^     {\lstinline[language=sml,style=15150code,#1]`#2`}%
^ \fi}}
^ % code block
^ \lstnewenvironment{codeblock}[1][]{\lstset{language=sml,style=15150code,#1}}{}
^ 
^ % code imported from a file
\newcommand{\codefile}[2][]{\lstinputlisting[language=sml,style=15150code,mathescape=false,frame=single,#1]{#2}}
^ \title{Datatypes}
^ \author{Dennis Chen}

\titlepage

~ Algebraic Data Types (ADT)

- Product type (AND)
- Sum type (OR)

\begin{codeblock}
(* Representation of items in a store.*)

(* Product type. *)
type item = int * string
(* Sum type. *)
datatype Identifier = Barcode of int | Name of string
\end{codeblock}

~ The product type

Think of it as AND; you need every piece of info

\code{val xbox : int * string = (400, "XBox")}

~ The sum type

Think of it as (exclusive) OR; you need exactly one piece of info

WARNING: This does not generalize so cleanly to logical OR, logical XOR, etc.

~ Why ``product''?

Assume for \code{type item = int * string}...

- 10 possible prices (\code{int})
- 10 possible names (\code{string})
- $10 \cdot 10 = 100$ possible \code{items} 

~ Why ``sum''?

Assume for \code{datatype Identifier = Barcode of int | Name of string}...

- 10 possible \code{Barcode}s
- 10 possible \code{Name}s
- $10 + 10 = 20$ possible \code{Identifiers}

(Sum types are also called tagged \emph{unions} --- why?)

~ SML syntax: \code{type} vs. \code{datatype}

- \code{type} is used for \emph{aliases}
- \code{datatype} is used whenever \emph{constructors} must be invoked

~ When is \code{type} and \code{datatype} used?

- \emph{In general}, \code{datatype} is used to construct sum types
- This is because in SML, \emph{sum types MUST come with constructors}

It's (almost) an if and only if relation:

\[\text{sum type} \iff \text{has constructors}\]

~ Inductive types

> Inductive type
>
> An \emph{inductive type} is any type that references itself at least once in its constructors.

- \emph{All inductive types are sum types}

~ Your favorite example, lists!

\begin{codeblock}
(* Declares :: as a right associative infix operator. *)
infixr 5 ::
datatype list = nil | :: of int * int list
\end{codeblock}

~ Trees \faTree

\begin{codeblock}
datatype tree = Empty | Node of tree * int * tree
\end{codeblock}

~ Induction review

- Base case: Prove a property $P$ holds for $0$
- Inductive case: $P(n) \implies P(n+1)$

So then you get

\[P(0) \implies P(1) \implies P(2) \implies \ldots\]

~ Recursive definitions

You can recursively characterize inductive datatypes:

- the size of \code{Empty} is $0$
- the size of \code{Tree} is the sum of the size of its children, plus $1$

...and you can write a recursive function to this effect.

\begin{codeblock}
fun size Empty = 0
 | size (Node(L, _, R)) = 1 + size L + size R
\end{codeblock}

~ Equality in trees is recursive

\begin{codeblock}
fun treeEq (Empty, Empty) = true
  | treeEq (Node(L0, x, R0), Node(L1, y, R1)) =
    x = y andalso treeEq(L0, L1) andalso treeEq(R0, R1)
  | treeEq (_, _) = false
\end{codeblock}

~ Structural induction

Leading question: how are instances of inductive datatypes generated?

- Base case: constructor does not involve recursive use of datatype
- Inductive case: constructor does involve recursive use of datatype

You recursively generate instance of the datatype in the inductive case!

~ Structurally inducting on trees \faTree

! Trees have non-negative size
! Show that $\code{size T} \geq 0$ for all trees $T$.

- Base case: \code{T = Empty}
- Inductive case: If \code{T = Node(L, x, R)} where $\code{size L} > 0$ and $\code{size R} > 0$, then \[\code{size T} = 1 + \code{size L} + \code{size R} \geq 1\]

~ Why did this work?

- Our base cases satisfy some property $P$
- Our inductive cases preserve $P$
- \emph{We can only ever generate trees that satisfy $P$}
- ...so all trees satisfy $P$.

~ Natural number induction is a special case of structural induction

Consider...

- Base case: $n = 0$
- Inductive case: $n \to n+1$

\begin{codeblock}
datatype Nat = Zero | Succ of Nat
\end{codeblock}

~ Questions \faQuestion

*questions-meme.jpg
